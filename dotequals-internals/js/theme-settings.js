$(document).ready( function() {
  // Hide the breadcrumb details, if no breadcrumb.
  $('#edit-dotequals-breadcrumb').change(
    function() {
      div = $('#div-dotequals-breadcrumb-collapse');
      if ($('#edit-dotequals-breadcrumb').val() == 'no') {
        div.slideUp('slow');
      } else if (div.css('display') == 'none') {
        div.slideDown('slow');
      }
    }
  );
  if ($('#edit-dotequals-breadcrumb').val() == 'no') {
    $('#div-dotequals-breadcrumb-collapse').css('display', 'none');
  }
  $('#edit-dotequals-breadcrumb-title').change(
    function() {
      checkbox = $('#edit-dotequals-breadcrumb-trailing');
      if ($('#edit-dotequals-breadcrumb-title').attr('checked')) {
        checkbox.attr('disabled', 'disabled');
      } else {
        checkbox.removeAttr('disabled');
      }
    }
  );
  $('#edit-dotequals-breadcrumb-title').change();
} );
